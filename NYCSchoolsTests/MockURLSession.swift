//
//  MockURLSession.swift
//  NYCSchoolsTests
//
//  Created by John Endres on 4/3/18.
//  Copyright © 2018 MacPlugins. All rights reserved.
//

import Foundation
@testable import NYCSchools

class MockURLSession: URLSessionProtocol {
    var nextDataTask = MockURLSessionDataTask()
//    var nextData: Data?
//    var nextError: Error?
    var responses = [(Data?, Error?)]()

    func addResponse(data: Data?, error: Error?) {
        responses.append((data, error))
    }

    func successHttpURLResponse(url: URL) -> URLResponse {
        return HTTPURLResponse(url: url, statusCode: 200, httpVersion: "HTTP/1.1", headerFields: nil)!
    }

    func dataTask(with url: URL, completionHandler: @escaping DataTaskResult) -> URLSessionDataTask {
        let (data, error) = responses.removeFirst()

        completionHandler(data, successHttpURLResponse(url: url), error)
        return nextDataTask
    }
}


class MockURLSessionDataTask: URLSessionDataTask {
    private (set) var resumeWasCalled = false

    override func resume() {
        resumeWasCalled = true
    }
}
