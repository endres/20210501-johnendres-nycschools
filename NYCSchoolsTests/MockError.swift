//
//  MockError.swift
//  NYCSchoolsTests
//
//  Created by John Endres on 4/4/18.
//  Copyright © 2018 MacPlugins. All rights reserved.
//

import Foundation

struct MockError: Error {
    var value = ""
}

extension MockError: Equatable {
    static func ==(lhs: MockError, rhs: MockError) -> Bool {
        return lhs.value == rhs.value
    }
}
