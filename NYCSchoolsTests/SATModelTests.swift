//
//  SATModelTests.swift
//  NYCSchoolsTests
//
//  Created by MLS Discovery on 4/3/18.
//  Copyright © 2018 MacPlugins. All rights reserved.
//

import XCTest
@testable import NYCSchools

class SATModelTests: XCTestCase {
    let smallArrayString = "[{\"dbn\":\"01M292\",\"num_of_sat_test_takers\":\"29\",\"sat_critical_reading_avg_score\":\"355\",\"sat_math_avg_score\":\"404\",\"sat_writing_avg_score\":\"363\",\"school_name\":\"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES\"},{\"dbn\":\"01M293\",\"num_of_sat_test_takers\":\"29\",\"sat_critical_reading_avg_score\":\"355\",\"sat_math_avg_score\":\"404\",\"sat_writing_avg_score\":\"363\",\"school_name\":\"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES\"}]"

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDownload() {
        let downloadExpectation = expectation(description: "download")
        let satUrl = URL(string: "https://data.cityofnewyork.us/resource/734v-jeq5.json")!
        let mockSession = MockURLSession()
        let downloader = Downloader(session: mockSession)
        
        mockSession.addResponse(data: smallArrayString.data(using: .utf8), error: nil)

        downloader.download(from: satUrl) { (scores: [SATScoreModelElement]?, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(scores)
            XCTAssertEqual(scores?.count, 2)

            downloadExpectation.fulfill()
        }

        waitForExpectations(timeout: 10)
    }

    func testErrorDownload() {
        let downloadExpectation = expectation(description: "download")
        let satUrl = URL(string: "https://data.cityofnewyork.us/resource/734v-jeq5.json")!
        let mockSession = MockURLSession()
        let mockError = MockError(value: "error")
        let downloader = Downloader(session: mockSession)

        mockSession.addResponse(data: nil, error: mockError)

        downloader.download(from: satUrl) { (scores: [SATScoreModelElement]?, error) in
            XCTAssertNotNil(error)
            XCTAssert(error is MockError)

            let testError = error! as! MockError

            XCTAssertEqual(testError.value, mockError.value)
            XCTAssertEqual(testError, mockError)
            XCTAssertNil(scores)

            downloadExpectation.fulfill()
        }

        waitForExpectations(timeout: 10)
    }

}
