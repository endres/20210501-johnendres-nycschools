//
//  TestDownload.swift
//  NYCSchoolsTests
//
//  Created by John Endres on 4/4/18.
//  Copyright © 2018 MacPlugins. All rights reserved.
//

import XCTest
@testable import NYCSchools

class DownloadTests: XCTestCase {
    let satUrl = URL(string: "https://data.cityofnewyork.us/resource/734v-jeq5.json")!
    let successResponse = HTTPURLResponse(url: URL(string: "https://www.apple.com")!, statusCode: 200, httpVersion: "HTTP/1.1", headerFields: nil)!
    let badResponse = HTTPURLResponse(url: URL(string: "https://www.apple.com")!, statusCode: 500, httpVersion: "HTTP/1.1", headerFields: nil)!
    let unknownResponse = URLResponse(url: URL(string: "https://www.apple.com")!, mimeType: "mime", expectedContentLength: 0, textEncodingName: "name")
    let randomData = "data".data(using: .utf8)!
    let sampleError = MockError(value: "john")

    var mockSession: MockURLSession!
    var subject: Downloader!
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        mockSession = MockURLSession()
        subject = Downloader(session: mockSession)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
        
    func testDownloadErrorEquatable() {
        XCTAssertEqual(DownloadError.noData, DownloadError.noData)
        XCTAssertEqual(DownloadError.unknownResponse, DownloadError.unknownResponse)
        XCTAssertEqual(DownloadError.badResponse(200), DownloadError.badResponse(200))
        XCTAssertNotEqual(DownloadError.badResponse(200), DownloadError.badResponse(400))
        XCTAssertNotEqual(DownloadError.unknownResponse, DownloadError.noData)
    }

    func testErrorStateWithNoData() {
        XCTAssertThrowsError(try subject.checkErrorState(data: nil, response: successResponse, error: nil)) { (error) -> Void in
            XCTAssertEqual(error as? DownloadError, DownloadError.noData)
        }

        XCTAssertThrowsError(try subject.checkErrorState(data: nil, response: successResponse, error: sampleError)) { (error) -> Void in
            XCTAssertNotEqual(error as? DownloadError, DownloadError.noData)
        }

        XCTAssertThrowsError(try subject.checkErrorState(data: nil, response: unknownResponse, error: sampleError)) { (error) -> Void in
            XCTAssertNotEqual(error as? DownloadError, DownloadError.noData)
        }
    }

    func testErrorStateWithError() {
        XCTAssertThrowsError(try subject.checkErrorState(data: nil, response: successResponse, error: sampleError)) { (error) -> Void in
            XCTAssertEqual(error as? MockError, sampleError)
        }

        XCTAssertThrowsError(try subject.checkErrorState(data: randomData, response: successResponse, error: sampleError)) { (error) -> Void in
            XCTAssertEqual(error as? MockError, sampleError)
        }

        XCTAssertThrowsError(try subject.checkErrorState(data: randomData, response: unknownResponse, error: sampleError)) { (error) -> Void in
            XCTAssertEqual(error as? MockError, sampleError)
        }
    }

    func testErrorStateWithBadResponse() {
        XCTAssertThrowsError(try subject.checkErrorState(data: nil, response: badResponse, error: nil)) { (error) -> Void in
            XCTAssertEqual(error as? DownloadError, DownloadError.badResponse(500))
        }

        XCTAssertThrowsError(try subject.checkErrorState(data: randomData, response: badResponse, error: nil)) { (error) -> Void in
            XCTAssertEqual(error as? DownloadError, DownloadError.badResponse(500))
        }
    }

    func testErrorStateWithUnknownResponse() {
        XCTAssertThrowsError(try subject.checkErrorState(data: nil, response: unknownResponse, error: nil)) { (error) -> Void in
            XCTAssertEqual(error as? DownloadError, DownloadError.unknownResponse)
        }

        XCTAssertThrowsError(try subject.checkErrorState(data: randomData, response: unknownResponse, error: nil)) { (error) -> Void in
            XCTAssertEqual(error as? DownloadError, DownloadError.unknownResponse)
        }
    }

    func testDownloadArray() throws {
        let downloadExpectation = expectation(description: "download array")
        let satArrayString = "[{\"dbn\":\"01M292\",\"num_of_sat_test_takers\":\"29\",\"sat_critical_reading_avg_score\":\"355\",\"sat_math_avg_score\":\"404\",\"sat_writing_avg_score\":\"363\",\"school_name\":\"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES\"},{\"dbn\":\"01M293\",\"num_of_sat_test_takers\":\"29\",\"sat_critical_reading_avg_score\":\"355\",\"sat_math_avg_score\":\"404\",\"sat_writing_avg_score\":\"363\",\"school_name\":\"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES\"}]"
        
        mockSession.addResponse(data: satArrayString.data(using: .utf8), error: nil)

        subject.download(from: satUrl) { (result: [SATScoreModelElement]?, error) in
            XCTAssertNil(error)
            XCTAssertEqual(result?.count, 2)
            
            downloadExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 10)
    }
    
    func testDownloadArrayWrongData() throws {
        let downloadExpectation = expectation(description: "download array")
        let satArrayString = "[{\"dbn\":\"01M292\",\"num_of_sat_test_takers\":\"29\",\"sat_critical_reading_avg_score\":\"355\",\"sat_math_avg_score\":\"404\",\"sat_writing_avg_score\":\"363\",\"school_name\":\"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES\"},{\"dbn\":\"01M293\",\"num_of_sat_test_takers\":\"29\",\"sat_critical_reading_avg_score\":\"355\",\"sat_math_avg_score\":\"404\",\"sat_writing_avg_score\":\"363\",\"school_name\":\"HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES\"}]"

        mockSession.addResponse(data: satArrayString.data(using: .utf8), error: nil)

        subject.download(from: satUrl) { (result: [SchoolModelElement]?, error) in
            XCTAssertNil(result)
            XCTAssertNotNil(error)

            downloadExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 10)
    }

}
