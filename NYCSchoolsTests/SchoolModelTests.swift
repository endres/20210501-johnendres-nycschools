//
//  SchoolModelTests.swift
//  NYCSchoolsTests
//
//  Created by John Endres on 4/4/18.
//  Copyright © 2018 MacPlugins. All rights reserved.
//

import XCTest
@testable import NYCSchools

class SchoolModelTests: XCTestCase {

    let smallArrayString = "[{\"academicopportunities1\":\"Free college courses at neighboring universities\",\"academicopportunities2\":\"International Travel, Special Arts Programs, Music, Internships, College Mentoring English Language Learner Programs: English as a New Language\",\"admissionspriority11\":\"Priority to continuing 8th graders\",\"admissionspriority21\":\"Then to Manhattan students or residents\",\"admissionspriority31\":\"Then to New York City residents\",\"attendance_rate\":\"0.970000029\",\"bbl\":\"1008420034\",\"bin\":\"1089902\",\"boro\":\"M\",\"borough\":\"MANHATTAN\",\"building_code\":\"M868\",\"bus\":\"BM1, BM2, BM3, BM4, BxM10, BxM6, BxM7, BxM8, BxM9, M1, M101, M102, M103, M14A, M14D, M15, M15-SBS, M2, M20, M23, M3, M5, M7, M8, QM21, X1, X10, X10B, X12, X14, X17, X2, X27, X28, X37, X38, X42, X5, X63, X64, X68, X7, X9\",\"census_tract\":\"52\",\"city\":\"Manhattan\",\"code1\":\"M64A\",\"community_board\":\"5\",\"council_district\":\"2\",\"dbn\":\"02M260\",\"directions1\":\"See theclintonschool.net for more information.\",\"ell_programs\":\"English as a New Language\",\"extracurricular_activities\":\"CUNY College Now, Technology, Model UN, Student Government, School Leadership Team, Music, School Musical, National Honor Society, The Clinton Post (School Newspaper), Clinton Soup (Literary Magazine), GLSEN, Glee\",\"fax_number\":\"212-524-4365\",\"finalgrades\":\"6-12\",\"grade9geapplicants1\":\"1515\",\"grade9geapplicantsperseat1\":\"19\",\"grade9gefilledflag1\":\"Y\",\"grade9swdapplicants1\":\"138\",\"grade9swdapplicantsperseat1\":\"9\",\"grade9swdfilledflag1\":\"Y\",\"grades2018\":\"6-11\",\"interest1\":\"Humanities & Interdisciplinary\",\"latitude\":\"40.73653\",\"location\":\"10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)\",\"longitude\":\"-73.9927\",\"method1\":\"Screened\",\"neighborhood\":\"Chelsea-Union Sq\",\"nta\":\"Hudson Yards-Chelsea-Flatiron-Union Square                                 \",\"offer_rate1\":\"Â—57% of offers went to this group\",\"overview_paragraph\":\"Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.\",\"pct_stu_enough_variety\":\"0.899999976\",\"pct_stu_safe\":\"0.970000029\",\"phone_number\":\"212-524-4360\",\"primary_address_line_1\":\"10 East 15th Street\",\"program1\":\"M.S. 260 Clinton School Writers & Artists\",\"requirement1_1\":\"Course Grades: English (87-100), Math (83-100), Social Studies (90-100), Science (88-100)\",\"requirement2_1\":\"Standardized Test Scores: English Language Arts (2.8-4.5), Math (2.8-4.5)\",\"requirement3_1\":\"Attendance and Punctuality\",\"requirement4_1\":\"Writing Exercise\",\"requirement5_1\":\"Group Interview (On-Site)\",\"school_10th_seats\":\"1\",\"school_accessibility_description\":\"1\",\"school_email\":\"admissions@theclintonschool.net\",\"school_name\":\"Clinton School Writers & Artists, M.S. 260\",\"school_sports\":\"Cross Country, Track and Field, Soccer, Flag Football, Basketball\",\"seats101\":\"YesÂ–9\",\"seats9ge1\":\"80\",\"seats9swd1\":\"16\",\"state_code\":\"NY\",\"subway\":\"1, 2, 3, F, M to 14th St - 6th Ave; 4, 5, L, Q to 14th St-Union Square; 6, N, R to 23rd St\",\"total_students\":\"376\",\"website\":\"www.theclintonschool.net\",\"zip\":\"10003\"},{\"academicopportunities1\":\"Learning to Work, Student Council, Advisory Leadership, School Newspaper, Community Service Group, School Leadership Team, Extended Day/PM School, College Now\",\"academicopportunities2\":\"CAMBA, Diploma Plus, Medgar Evers College, Coney Island Genera on Gap, Urban Neighborhood Services, Coney Island Coalition Against Violence, I Love My Life Initiative, New York City Police Department\",\"academicopportunities3\":\"The Learning to Work (LTW) partner for Liberation Diploma Plus High School is CAMBA.\",\"addtl_info1\":\"The Learning to Work (LTW) program assists students in overcoming obstacles that impede their progress toward a high school diploma. LTW offers academic and student support, career and educational exploration, work preparation, skills development, and internships that prepare students for rewarding employment and educational experiences after graduation. These LTW supports are provided by a community-based organization (CBO) partner.\",\"attendance_rate\":\"0.550000012\",\"bbl\":\"3070200039\",\"bin\":\"3329331\",\"boro\":\"K\",\"borough\":\"BROOKLYN \",\"building_code\":\"K728\",\"bus\":\"B36, B64, B68, B74, B82, X28, X38\",\"census_tract\":\"326\",\"city\":\"Brooklyn\",\"code1\":\"L72A\",\"community_board\":\"13\",\"council_district\":\"47\",\"dbn\":\"21K728\",\"directions1\":\"Students must attend an Open House and personalized intake meeting. To find out about open houses, students should call the school at 718-946-6812 or visit our website.\",\"eligibility1\":\"For Current 8th Grade Students Â– Open only to students who are at least 15 1/2 years of age and entering high school for the first time. For Other Students Â– Open only to students who are at least 16 years of age and have attended another high school for at least one year\",\"ell_programs\":\"English as a New Language\",\"extracurricular_activities\":\"Advisory Leadership, Student Council, Community Service Leadership, School Leadership Team, A er-School Enrichment, Peer Tutoring, School Newspaper\",\"fax_number\":\"718-946-6825\",\"finalgrades\":\"School is structured on credit needs, not grade level\",\"grade9geapplicants1\":\"N/A\",\"grade9geapplicantsperseat1\":\"N/A\",\"grade9gefilledflag1\":\"N/A\",\"grade9swdapplicants1\":\"N/A\",\"grade9swdapplicantsperseat1\":\"N/A\",\"grade9swdfilledflag1\":\"N/A\",\"grades2018\":\"School is structured on credit needs, not grade level\",\"interest1\":\"Humanities & Interdisciplinary\",\"language_classes\":\"French, Spanish\",\"latitude\":\"40.57698\",\"location\":\"2865 West 19th Street, Brooklyn, NY 11224 (40.576976, -73.985413)\",\"longitude\":\"-73.9854\",\"method1\":\"Limited Unscreened\",\"neighborhood\":\"Seagate-Coney Island\",\"nta\":\"Seagate-Coney Island                                                       \",\"overview_paragraph\":\"The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally. We will equip students with the skills needed to evaluate their options so that they can make informed and appropriate choices and create personal goals for success. Our year-round model (trimesters plus summer school) provides students the opportunity to gain credits and attain required graduation competencies at an accelerated rate. Our partners offer all students career preparation and college exposure. Students have the opportunity to earn college credit(s). In addition to fulfilling New York City graduation requirements, students are required to complete a portfolio to receive a high school diploma.\",\"pct_stu_enough_variety\":\"0.899999976\",\"pct_stu_safe\":\"0.959999979\",\"phone_number\":\"718-946-6812\",\"primary_address_line_1\":\"2865 West 19th Street\",\"program1\":\"Liberation Diploma Plus High School\",\"school_10th_seats\":\"1\",\"school_email\":\"scaraway@schools.nyc.gov\",\"school_name\":\"Liberation Diploma Plus High School\",\"school_sports\":\"Basketball \",\"seats101\":\"Yes-New\",\"seats9ge1\":\"N/A\",\"seats9swd1\":\"N/A\",\"state_code\":\"NY\",\"subway\":\"D, F, N, Q to Coney Island  Â–  S llwell Avenue\",\"total_students\":\"206\",\"transfer\":\"1\",\"website\":\"schools.nyc.gov/schoolportals/21/K728\",\"zip\":\"11224\"}]"

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testDownload() {
        let downloadExpectation = expectation(description: "download")
        let satUrl = URL(string: "https://data.cityofnewyork.us/resource/97mf-9njv.json")!
        let mockSession = MockURLSession()
        let downloader = Downloader(session: mockSession)

        mockSession.addResponse(data: smallArrayString.data(using: .utf8), error: nil)

        downloader.download(from: satUrl) { (schools: [SchoolModelElement]?, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(schools)

            downloadExpectation.fulfill()
        }

        waitForExpectations(timeout: 10)
    }

    func testErrorDownload() {
        let downloadExpectation = expectation(description: "download")
        let satUrl = URL(string: "https://data.cityofnewyork.us/resource/97mf-9njv.json")!
        let mockSession = MockURLSession()
        let downloader = Downloader(session: mockSession)
        let mockError = MockError(value: "error")

        mockSession.addResponse(data: nil, error: mockError)

        downloader.download(from: satUrl) { (scores: [SchoolModelElement]?, error) in
            XCTAssertNotNil(error)
            XCTAssert(error is MockError)

            let testError = error! as! MockError

            XCTAssertEqual(testError.value, mockError.value)
            XCTAssertEqual(testError, mockError)
            XCTAssertNil(scores)

            downloadExpectation.fulfill()
        }

        waitForExpectations(timeout: 10)
    }

}
