//
//  MasterViewController.swift
//  NYCSchools
//
//  Created by MLS Discovery on 4/3/18.
//  Copyright © 2018 MacPlugins. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var session: URLSessionProtocol = URLSession.shared
    var detailViewController: DetailViewController? = nil
    var schools = SchoolModel()
    var scores = SATScoreModel()
    var activityIndicatorView = UIActivityIndicatorView(style: .gray)
    
    /// Wrapper around stopActivity(), the function that really cleans up.  This wrapper is used to force stopActivity() onto
    /// the main queue, and that allows test cases to be written around stopActivity()
    func showData() {
        DispatchQueue.main.async {
            self.stopActivity()
        }
    }

    /// Naive error reporter.  Currently just prints to console
    /// - Parameter error: error to show, nil if none
    /// - Returns: true if error, false if not
    func showError(error: Error?) -> Bool {
        if let error = error {
            DispatchQueue.main.async {
                self.stopActivity()
                print("error loading data: \(error)")
            }
            
            return false
        }

        return true
    }
    
    /// Bulk download from hard-coded strings.  Obviously a bad approach, and you can see older sources in the comments.
    /// Really this should look at the API better to find a way to query for current results.  Felt like registering an account,
    /// generating tokens, etc. is out of scope and would only illustrate the same things the base of this code does.
    func loadData() {
        let schoolDataLocation = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        // let schoolDataLocation = "https://data.cityofnewyork.us/resource/97mf-9njv.json"
        
        if let satUrl = URL(string: schoolDataLocation) {
            let schoolDownloader = Downloader(session: session)
            
            schoolDownloader.download(from: satUrl) { (schools: [SchoolModelElement]?, error) in
                guard self.showError(error: error) else {
                    return
                }

                if let schools = schools {
                    self.schools = schools.sorted(by: { (lhs, rhs) -> Bool in
                        return lhs.schoolName < rhs.schoolName
                    })
                }

                let satDataLocation = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
                // let satDataLocation = "https://data.cityofnewyork.us/resource/734v-jeq5.json"
                
                if let scoreURL = URL(string: satDataLocation) {
                    let satDownloader = Downloader(session: self.session)

                    satDownloader.download(from: scoreURL) { (scores: [SATScoreModelElement]?, error) in
                        guard self.showError(error: error) else {
                            return
                        }

                        if let scores = scores {
                            self.scores = scores
                            self.showData()
                        }
                    }
                }
            }
        }
    }
    
    /// Put the view into a "loading" mode while the data is in transit.  I think for this to work correctly that user interaction might need disabling as well.
    func startActivity() {
        activityIndicatorView.startAnimating()

        tableView.backgroundView = activityIndicatorView
        tableView.separatorStyle = .none
    }
    
    /// Take the view out of the "loading" mode.
    func stopActivity() {
        activityIndicatorView.stopAnimating()
        
        tableView.backgroundView = nil
        self.tableView.separatorStyle = .singleLine
        self.tableView.reloadData()
    }
    
    /// Loads the view by putting it into a loading mode, setting up the default and canned split view, and then starting the download
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view, typically from a nib.
        startActivity()

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }

        loadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    // MARK: - Segues
    
    /// Prepare the detail view with the proper school information
    /// - Parameters:
    ///   - segue: Segue in question, the identifier must be "showDetail" for this to work
    ///   - sender: Sender of the segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let districtBoroughNumber = schools[indexPath.row].dbn
                let something = scores.filter { (score) -> Bool in
                    return score.dbn == districtBoroughNumber
                }

                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController

                controller.detailItem = something.first
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return scores.count > 0 ? 1 : 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object = schools[indexPath.row]
        cell.textLabel!.text = object.schoolName
        return cell
    }

}

