//
//  URLSessionProtocol.swift
//  NYCSchools
//
//  Created by MLS Discovery on 4/3/18.
//  Copyright © 2018 MacPlugins. All rights reserved.
//

import Foundation

/// Abstraction layer to allow for mocking in the unit tests
protocol URLSessionProtocol {
    typealias DataTaskResult = (Data?, URLResponse?, Error?) -> Void

    func dataTask(with url: URL, completionHandler: @escaping DataTaskResult) -> URLSessionDataTask
}

/// Abstraction layer to allow for mocking in the unit tests
protocol URLSessionDataTaskProtocol {
    func resume()
}

/// Abstraction layer to allow for mocking in the unit tests
extension URLSessionDataTask: URLSessionDataTaskProtocol {
}

/// Abstraction layer to allow for mocking in the unit tests
extension URLSession: URLSessionProtocol {
//    func dataTask(with url: URL, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
//        return dataTask(with: url, completionHandler: completionHandler) as URLSessionDataTaskProtocol
//    }
}
