//
//  Download.swift
//  NYCSchools
//
//  Created by John Endres on 4/3/18.
//  Copyright © 2018 MacPlugins. All rights reserved.
//

import Foundation

/// Internal errors this module handles
enum DownloadError: Error {
    case noData
    case unknownResponse
    case badResponse(Int)
}

extension DownloadError: Equatable {
    static func ==(lhs: DownloadError, rhs: DownloadError) -> Bool {
        switch (lhs, rhs) {
        case (.noData, .noData),
             (.unknownResponse, .unknownResponse):
            return true
        case let (.badResponse(l), .badResponse(r)):
            return l == r
        default:
            return false
        }
    }
}

/// Naive wrapper around dataTask, session, JSONDecoder and error handling for downloading an array of json data.
class Downloader {
    /// Session instance to use when downloading
    let session: URLSessionProtocol
    
    /// Setup the Downloader instance
    /// - Parameters:
    ///   - session: Session to use for downloading that data
    init(session: URLSessionProtocol=URLSession.shared) {
        self.session = session
    }
    
    /// High-level function to check the simple error states from the download task.  Tries to extract out
    /// HTTP Error codes
    /// - Parameters:
    ///   - data: data instance from callback
    ///   - response: response from callback
    ///   - error: error from callback
    /// - Throws: Either one of hte internal errors or just the provided error
    internal func checkErrorState(data: Data?, response: URLResponse?, error: Error?) throws {
        if let error = error {
            // Really, maybe this should be an extra case in DownloadError for consistency
            throw error
        }

        // Paranoia
        guard let httpResponse = response as? HTTPURLResponse else {
            throw DownloadError.unknownResponse
        }

        // Could be too broad, but a success is a success.  Lack of data will/should
        // be handled in the completion block that calls this.  This is to propagate
        // the HTTP response code
        if !(200...299).contains(httpResponse.statusCode) {
            throw DownloadError.badResponse(httpResponse.statusCode)
        }

        // Finally, lack of data is an error if there is also a lack of an error
        if data == nil {
            throw DownloadError.noData
        }
    }
    
    /// Performs the data request for the given URL using the given Session in the object
    /// - Parameter url: source of data
    /// - Parameter completion: Result of the data task
    func download<T: Decodable>(from url: URL, completion: @escaping ([T]?, Error?) -> Void) {
        let task = session.dataTask(with: url) { [self] (data, response, error) in
            do {
                try checkErrorState(data: data, response: response, error: error)

                let result = try JSONDecoder().decode([T].self, from: data!)

                completion(result, nil)
            } catch {
                completion(nil, error)
            }
        }

        task.resume()
    }
    
}
