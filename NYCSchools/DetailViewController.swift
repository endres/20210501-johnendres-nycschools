//
//  DetailViewController.swift
//  NYCSchools
//
//  Created by MLS Discovery on 4/3/18.
//  Copyright © 2018 MacPlugins. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var dbnLabel: UILabel!
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var participantsLabel: UILabel!
    @IBOutlet weak var readingLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!


    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            guard dbnLabel != nil else {
                return
            }
            
            dbnLabel.text = detail.dbn
            schoolNameLabel.text = detail.schoolName
            participantsLabel.text = detail.numOfSatTestTakers
            readingLabel.text = detail.satCriticalReadingAvgScore
            mathLabel.text = detail.satMathAvgScore
            writingLabel.text = detail.satWritingAvgScore
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: SATScoreModelElement? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

