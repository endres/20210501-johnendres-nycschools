//
//  SATScoreModel.swift
//  NYCSchools
//
//  Created by John Endres on 4/3/18.
//  Copyright © 2018 MacPlugins. All rights reserved.
//

import Foundation

// NOTE: The base of this comes from https://app.quicktype.io with some extensions to fit with my protocols

typealias SATScoreModel = [SATScoreModelElement]

struct SATScoreModelElement: Codable {
    let dbn: String
    let numOfSatTestTakers: String
    let satCriticalReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String
    let schoolName: String

    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
        case schoolName = "school_name"
    }
}
